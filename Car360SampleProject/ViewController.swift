//
//  ViewController.swift
//  Car360SampleProject
//
//  Created by Remy Cilia on 5/11/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

import UIKit
import Car360Framework
import MBProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CaptureViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var stableIds = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Car360Framework.frameworkActive {
            self.updateSpinsList()
        } else {
            Car360Framework.initFramework(username: "remy@egosventures.com", password: "car360Admin", completionBlock: { (activated) -> (Void) in
                print("framework activated? \(activated)")
                if activated {
                    self.updateSpinsList()
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openCar360CaptureView(_ sender: Any) {
        // get the capture view and present it
        
        if let captureVC = Car360Framework.getCaptureViewController(delegate: self, defaultCaptureType: .walkAround, carIdentificationType: .vin, carIdentification: "MY_VIN_1234", carCondition: .used) {
            self.present(captureVC, animated: true, completion: nil)
        }
    }
    
    func updateSpinsList() {
        // listLocalStableIds returns the list of spins saved on the phone, sorted by date taken inverted (most recent first)
        self.stableIds = Car360Framework.listLocalStableIds()
        self.tableView.reloadData()
    }
    
    // MARK: - Actions
    @IBAction func uploadAction(_ sender: AnyObject) {
        guard let button = sender as? UIButton,
            let tableView = self.tableView,
            let indexPath = getIndexPathFromHitPoint(hitPoint: button.convert(CGPoint.zero, to: tableView)) else {
                return
        }
        
        if self.stableIds.count > indexPath.row {
            Car360Framework.uploadSpin(
                stableId: self.stableIds[indexPath.row],
                customerIntegration: nil,
                startBlock: { (stableId) -> (Void) in
                    print("start uploading \(stableId)")
                },
                progressBlock: { (stableId, uploadedMB, totalMB) -> (Void) in
                    print("upload progress: \(uploadedMB) / \(totalMB) (\(uploadedMB*100/totalMB)%)")
                }, completionBlock: { (stableId, spinCode, uploadSucceded, errorObject) -> (Void) in
                    if uploadSucceded {
                        print("upload finished successfully. SpinCode: \(spinCode)")
                    } else {
                        print("upload failed: \(errorObject?.errorMessage)")
                    }
                }
            )
        }
    }
    
    @IBAction func deleteAction(_ sender: AnyObject) {
        guard let button = sender as? UIButton,
            let tableView = self.tableView,
            let indexPath = getIndexPathFromHitPoint(hitPoint: button.convert(CGPoint.zero, to: tableView)) else {
                return
        }
        
        if self.stableIds.count > indexPath.row {
            Car360Framework.deleteSpin(stableId: self.stableIds[indexPath.row])
            self.updateSpinsList()
        }
    }
    
    func getIndexPathFromHitPoint(hitPoint: CGPoint) -> IndexPath? {
        return self.tableView.indexPathForRow(at: hitPoint)
    }
    
    // MARK: CaptureViewControllerDelegate methods
    func captureViewController(_ captureViewController: CaptureViewController, didFinishCapturingWithStableId stableId: String?, carIdentificationType: CarIdType, carIdentification: String?, spinSaved: Bool, canceled: Bool) {
        
        if spinSaved {
            print("spin has been saved with id: \(stableId) (carIdentification: \(carIdentification))")
        } else {
            print("spin has not been saved (carIdentification: \(carIdentification))")
        }
    }
    
    // MARK: UITableViewDelegate methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard self.stableIds.count > indexPath.row else {
            return
        }
        
        if let viewerVC = Car360Framework.getLocalViewerViewController(stableId: self.stableIds[indexPath.row], buttonsToShow: ViewerButtons.allValues as NSArray?) {
            self.present(viewerVC, animated: true, completion: nil)
        }
    }
    
    // MARK: UITableViewDataSource methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "stableCell") {
            if self.stableIds.count > indexPath.row {
                if let label = cell.contentView.viewWithTag(1000) as? UILabel {
                    label.text = self.stableIds[indexPath.row]
                }
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stableIds.count
    }
}

