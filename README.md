# CAR360 Sample App #

A sample app showing how to use the CAR360 framework

### How do I get set up? ###

* Create a "Frameworks" folder into "Car360SampleProject"
* Copy "Car360Framework.framework" and "Stable360Engine.framework" into the "Frameworks" folder
* Open "Car360SampleProject.xcworkspace"
* Update your login and password in ViewController.swift
* Run the app (on a device only)

### Who do I talk to? ###

* [bruno@egosventures.com](mailto:bruno@egosventures.com)
* [remy@egosventures.com](mailto:remy@egosventures.com)
* [Car360.net](http://car360.net)
* [Car360 iOS framework documentation](http://doc.car360app.com/iOS/capture/Classes/Car360Framework.html)